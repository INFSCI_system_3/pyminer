文档编译工具
============

这个包用于进行文档的编译工作，是 ``sphinx-apidoc`` 的替代品，其主要思路如下：

#. 将 ``*.py`` 文件按照 ``jinja`` 模板生成 ``*.rst`` 文件；
#. 将 ``*.rst`` 文件采用 ``sphinx`` 生成 ``*.html`` 文件。

本包的主要内容为根据 ``*.py`` 文件生成 ``*.rst`` 文件。

根据 ``*.rst`` 文件仍为直接调用 ``sphinx`` 生成。

由于暂时仅作为 ``pyminer`` 的内部工具，暂不考虑复用性。

定义了两个结构：

#. ``Node`` ，用于进行python文件夹下的文件结构的遍历；
#. ``RstGenerator`` ，用于根据python文件结构生成rst文件结构。

TODO 具体的使用说明有待补全

识别Python文件结构
--------------------------

.. autoclass:: pyminer_devutils.doc.FileTreeNode
    :members:
    :undoc-members:

生成rst文件结构
-----------------------------------------

.. autoclass:: pyminer_devutils.doc.RstGenerator
    :members:
    :undoc-members:

.. toctree::
    :maxdepth: 2

    file_tree.rst
    rst_generator.rst