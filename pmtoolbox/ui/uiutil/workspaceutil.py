from typing import TYPE_CHECKING

from qtpy.QtWidgets import QComboBox, QInputDialog, QWidget, QLineEdit, QMessageBox


def input_identifier(parent: QWidget, default_name: str = '') -> str:
    """
    要求用户输入一个变量名
    :param default_name: 
    :return: 
    """
    from pyminer_algorithms import get_var_names
    assert default_name.isidentifier()
    var_names = get_var_names()
    if var_names is None:
        var_names = []
    while (1):
        name, ok = QInputDialog.getText(parent, "变量命名", "输入新的变量名称:", QLineEdit.Normal, default_name)
        print(ok)
        if ok and (len(name) != 0):

            if name in var_names:
                QMessageBox.warning(parent, "提示", "变量名已存在")
                continue
            elif not name.isidentifier():
                QMessageBox.warning(parent, '提示', '变量名无效\n提示：\n1、不要以数字开头;\n2、不要包含除下划线外的所有符号。')
                continue
            else:
                return name
        else:
            return ''

def bind_combo_with_workspace(combo: QComboBox, type_filter: str = ''):
    """
    将combobox与工作空间的变量变更绑定起来。自动获取相应的变量名。

    :param combo:
    :param type_filter:          变量类型的字符表示
        目前支持四种：string,table,array和numeric。使用table可以过滤出所有的二维array\pd.DataFrame
        默认值为‘’也就是空字符串，此时将返回所有的变量名。
    :return:
    """
    from pyminer_algorithms import get_var_names

    def on_combo_var_name_mouse_pressed(event):
        """
        combo box在点击（菜单未弹出）的时候，从PyMiner主程序获取全部符合要求的变量名。
        同时需要调用QComboBox的mousePressEvent事件，保证选单正常弹出。
        :return:
        """
        var_names = get_var_names(type_filter=type_filter)

        last_item_text = combo.currentText()

        combo.clear()
        combo.addItems(var_names)
        if last_item_text in var_names:
            combo.setCurrentIndex(var_names.index(last_item_text))
        QComboBox.mousePressEvent(combo, event)

    combo.mousePressEvent = on_combo_var_name_mouse_pressed
