pmgwidgets
==============================================================================

.. automodule:: pmgwidgets
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2

   display/index.rst
   elements/index.rst
   flowchart/index.rst
   utilities/index.rst
   widgets/index.rst

一些流浪的页面
----------------

.. toctree::
    :maxdepth: 2

    README.md
    flowchart/readme.md
    flowchart/readme_arch.md
    flowchart/创建新节点.md
    docs/threading&tasking.md
