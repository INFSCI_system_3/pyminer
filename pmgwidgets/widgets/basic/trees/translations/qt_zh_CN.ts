<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="zh_CN" sourcelanguage="">
<context>
    <name>PMFileSystemModel</name>
    <message>
        <location filename="../filetree.py" line="19"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="21"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="23"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="25"/>
        <source>Last Modified</source>
        <translation>上次修改</translation>
    </message>
</context>
<context>
    <name>PMGAttrTree</name>
    <message>
        <location filename="../varattrtree.py" line="30"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../varattrtree.py" line="32"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../varattrtree.py" line="34"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../varattrtree.py" line="47"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="../varattrtree.py" line="48"/>
        <source>Save as </source>
        <translation>保存为</translation>
    </message>
    <message>
        <location filename="../varattrtree.py" line="49"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="../varattrtree.py" line="50"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="../varattrtree.py" line="51"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>PMGFilesTreeview</name>
    <message>
        <location filename="../filetree.py" line="125"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="127"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="130"/>
        <source>New..</source>
        <translation>新建..</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="132"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="133"/>
        <source>Folder</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="140"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="141"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="245"/>
        <source>Please Input file name</source>
        <translation>请输入文件名</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="309"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="177"/>
        <source>Folder name %s is illeagal!</source>
        <translation>文件夹名称%s不合规范!</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="188"/>
        <source>Folder %s already exists!</source>
        <translation>文件夹%s 已经存在!</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="211"/>
        <source>File %s already exists!</source>
        <translation>文件%s已经存在!</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="250"/>
        <source>Unable to Rename this file.</source>
        <translation>无法重命名这个文件。</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="265"/>
        <source>Unable to Move this file to recycle bin.</source>
        <translation>无法将这个文件移动到回收站。</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="136"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="137"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="309"/>
        <source>Copy File or Directory Error.</source>
        <translation>无法复制文件或文件夹。</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="142"/>
        <source>Filter</source>
        <translation>过滤文件类型</translation>
    </message>
    <message>
        <location filename="../filetree.py" line="317"/>
        <source>Extension Name To Show</source>
        <translation>显示的文件类型</translation>
    </message>
</context>
<context>
    <name>PMGJsonTree</name>
    <message>
        <location filename="../jsontree.py" line="33"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../jsontree.py" line="35"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../jsontree.py" line="47"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="../jsontree.py" line="48"/>
        <source>Save as </source>
        <translation>储存为</translation>
    </message>
    <message>
        <location filename="../jsontree.py" line="49"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="../jsontree.py" line="50"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="../jsontree.py" line="51"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>app</name>
    <message>
        <location filename="../treecheck.py" line="75"/>
        <source>Program Scripts</source>
        <translation>程序脚本</translation>
    </message>
    <message>
        <location filename="../treecheck.py" line="76"/>
        <source>Documents</source>
        <translation>办公文件</translation>
    </message>
    <message>
        <location filename="../treecheck.py" line="77"/>
        <source>Data Files</source>
        <translation>数据文件</translation>
    </message>
</context>
</TS>
