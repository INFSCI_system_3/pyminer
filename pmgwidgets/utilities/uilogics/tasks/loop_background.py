# coding=utf-8
__author__ = '侯展意'

import logging
import sys
import time
from typing import Callable, Tuple, Iterable
# from collections import Iterable
from qtpy.QtGui import QCloseEvent
from qtpy.QtWidgets import QApplication
from qtpy.QtCore import QObject, Signal

logger = logging.getLogger(__name__)
from pmgwidgets.utilities.uilogics.tasks.minimal_thread import PMGQThreadManager


class PMGEndlessLoopWorker(QObject):
    """
    在循环中工作。
    输入参数：单步工作函数；参数（可迭代对象）。
    """
    signal_step_finished = Signal(object)
    signal_finished = Signal()

    def __init__(self, work_fcn: Callable, args: Iterable = None):
        super(PMGEndlessLoopWorker, self).__init__()
        self.quit = False
        self.args = args
        self.work_fcn: Callable = work_fcn

    def work(self):
        assert callable(self.work_fcn)

        while (1):
            step_ret = self.work_fcn(*self.args)
            self.signal_step_finished.emit(step_ret)
            if self.quit:
                break
        self.signal_finished.emit()

    def on_exit(self):
        self.quit = True


class PMGLoopWorker(QObject):
    """
    在循环中工作。
    第一种方法可以传入可迭代对象作为 iter_args 参数，循环次数就是 iter_args 的长度。此种情况下loop_times和step_args无效。
    第二种方法，当iter_args为None的时候，传入循环次数loop_times作为参数，每一步分别的参数由step_args传入。
    输入参数：单步工作函数；参数（可迭代对象）。
    """
    signal_step_finished = Signal(int, object)
    signal_finished = Signal()

    def __init__(self, work_fcn: Callable, iter_args: Iterable = None, loop_times: int = 100,
                 step_args: Iterable = None):
        super(PMGLoopWorker, self).__init__()
        self.quit = False

        self.iter_args = iter_args if iter_args is not None else [([] if step_args is None else step_args) for i in
                                                                  range(loop_times)]
        self.work_fcn: Callable = work_fcn

    def work(self):
        assert callable(self.work_fcn)
        assert self.iter_args is not None
        for i, step_args in enumerate(self.iter_args):
            step_ret = self.work_fcn(*step_args)
            self.signal_step_finished.emit(i, step_ret)
        self.signal_finished.emit()

    def on_exit(self):
        self.quit = True


class PMGLoopThreadRunner(QObject):
    signal_finished = Signal()
    signal_step_finished = Signal(int, object)

    def __init__(self, callback: Callable, iter_args: Iterable = None, loop_times: int = 100,
                 step_args: Iterable = None):
        super().__init__()
        self.worker = PMGLoopWorker(callback, iter_args, loop_times, step_args)
        self.thread_mgr = PMGQThreadManager(worker=self.worker)
        self.worker.signal_step_finished.connect(self.signal_step_finished.emit)
        self.worker.signal_finished.connect(self.signal_finished.emit)


class PMGEndlessLoopThreadRunner(QObject):
    signal_finished = Signal()
    signal_step_finished = Signal(object)

    def __init__(self, callback: Callable, args: Iterable = None):
        super().__init__()
        self.worker = PMGEndlessLoopWorker(callback, args=args)
        self.thread_mgr = PMGQThreadManager(worker=self.worker)
        self.worker.signal_step_finished.connect(self.signal_step_finished.emit)
        self.worker.signal_finished.connect(self.signal_finished.emit)


if __name__ == '__main__':
    from qtpy.QtWidgets import QTextEdit


    def run(i, j):
        time.sleep(0.1)
        return i + j


    class TextEdit(QTextEdit):
        def __init__(self):
            super(TextEdit, self).__init__()
            self.oneshot = PMGLoopThreadRunner(run, iter_args=[(i, i + 1) for i in range(100)])
            self.oneshot.signal_step_finished.connect(self.on_step_finished)
            self.oneshot.signal_finished.connect(self.on_finished)

        def on_step_finished(self, step, result):
            self.append('step:%d,result:%s\n' % (step, repr(result)))

        def on_finished(self):
            self.append('finished!')

        def closeEvent(self, a0: 'QCloseEvent') -> None:
            super().closeEvent(a0)


    app = QApplication(sys.argv)
    text = TextEdit()
    text.show()
    sys.exit(app.exec_())
