import sys
import time

from pmgwidgets import PMGOneShotThreadRunner  # 导入单线程任务运行器
from qtpy.QtWidgets import QTextEdit, QApplication


def run(loop_times):
    """
    任务函数。在函数执行之后，发出signal_finished事件，返回一个参数。如果函数有多个返回参数，则返回参数元组。
    :param loop_times:
    :return:
    """
    for i in range(loop_times):
        print(i)
        time.sleep(1)
    return 'finished!!', 'aaaaaa', ['finished', 123]


app = QApplication(sys.argv)
text = QTextEdit()
text.show()
oneshot = PMGOneShotThreadRunner(run, args=(5,))
oneshot.signal_finished.connect(lambda x: text.append('任务完成，函数返回值：' + repr(x)))
sys.exit(app.exec_())
