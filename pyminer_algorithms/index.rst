PyMiner 算法库
==============================================================================

.. toctree::
   :maxdepth: 2

   linear_algebra/index.rst
   plotting/index.rst
   pyminer_util/index.rst
   statistics/index.rst
   linear_algebra/开发流程.md
