"""
这是一个用来调用，自动生成翻译文件的脚本。
"""
import os, sys
# import pmgwidgets
# sys.exit(0)
pn = os.path.dirname
pj = os.path.join
root = pn(pn(__file__))
print(root)
designer_root = pj(root, r'attachments', 'qtdesigner')
print(designer_root)
pylupdate_path = r'J:\Developing\pyminer_bin\PyMiner\interpreter\python.exe J:/Developing/pyminer_bin/PyMiner/bin/pmtoolbox/ui/translation.py'  # pj(root, r'interpreter', 'Scripts', 'pylupdate5.exe')
linguist_path = pj(designer_root, r'linguist.exe')
import sys, subprocess
import os
import shutil
import os

path1 = r'E:\Python\pyminer_bin\PyMiner\bin\pmgwidgets'
path2 = r'E:\Python\pyminer_bin\PyMiner\bin\pmgwidgets'
s = ''


def update_ts(packages_path):
    l = os.listdir(packages_path)
    for folder in l:
        if folder.find('.') != -1:
            continue
        abso_path = os.path.join(packages_path, folder)
        translation_folder = os.path.join(abso_path, 'translations')
        if not os.path.exists(translation_folder):
            os.mkdir(translation_folder)
        filenames = ''
        for filename in os.listdir(abso_path):
            if filename.endswith('.py'):
                file_abso_path = os.path.join(abso_path, filename)
                filenames += ' ' + file_abso_path + ' '
        translation_file = os.path.join(translation_folder, 'qt_zh_CN.ts')
        if filenames.strip() !='':
            cmd = '%s %s -ts %s' % (pylupdate_path, filenames, translation_file)
        print(cmd)
        os.system(cmd)
        # cmd = 'cd %s && linguist %s' % (designer_root, os.path.normcase(translation_file))
        # print(cmd)
        # subprocess.Popen(cmd,shell=True)
        # plugins_path, platform_plugin_path = os.environ.pop('QT_PLUGIN_PATH'), os.environ.pop(
        #     'QT_QPA_PLATFORM_PLUGIN_PATH')  # =plugins_path

        # =platform_plugin_path

        # os.chdir(designer_root)
        # print(os.getcwd())
        # os.system(cmd)
        # os.environ['QT_PLUGIN_PATH'] = plugins_path
        # os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = platform_plugin_path


def update_translation(path):
    global s
    # print(path,os.path.exists(path))
    for root, dirs, files in os.walk(path, True):
        # print(root,path)
        for f in files:
            if f.endswith('.py') and not f.find('__init__.py'):
                file = os.path.join(root, f)
                s = s + ' ' + file + ' '


packages1_path = os.path.join(os.path.dirname(__file__), 'pyminer2/extensions/packages')
update_ts(packages1_path)
update_ts(os.path.join(os.path.dirname(__file__), r'pmgwidgets/widgets/basic'))
