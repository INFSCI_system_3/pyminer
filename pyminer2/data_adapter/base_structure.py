from functools import cached_property
from typing import Any, Tuple, List, Dict, Union

SELECTOR_TYPE = Union[Tuple[Union[int, None], Union[int, None], Union[int, None]], int]


class BaseAdapter(object):
    """
    数据适配器的基类。

    数据适配器的意义在于，将逻辑与界面进行分离。
    工作空间中的每一个数据都将是一个数据适配器的实例。
    这就确保了所有的数据接口统一，可以方便地用于在界面中进行显示，
    而不必要在界面中加入逻辑进行判断。

    在这个类中采用了缓存属性的方案，使得一些计算耗时比较长的内容可以仅在需要的时候才进行计算。

    所有在 ``pyminer`` 中进行跨线程、跨进程、跨插件的数据交换功能都应当采用这个类的子类的实例进行传输。

    目前（2020/11/22）这个适配器仍是一个愿景，各个插件依旧各自为战，后面需要进行整合。
    """
    data: Any  # 请各个子适配器都定义这个data的类型，以便于IDE进行类型提示

    def __init__(self, data: Any):
        self.data = data

    @cached_property
    def shape(self) -> Tuple[int]:
        raise NotImplementedError

    @cached_property
    def abstract(self) -> Dict[str, Any]:
        """
        摘要，用于实现元数据的快速传输
        """
        return {
            'shape': list(self.shape)
        }

    @cached_property
    def serialized_data(self):
        """
        实现实际存储的数据的序列化。

        数据转变为字符串是通过 ``json.dumps`` 实现的，
        这个接口只需要保证返回值是 ``json.dumps`` 可以解析的内容即可，
        这个接口的具体实现不需要调用 ``json.dumps`` 。

        这个接口仅处理数据本身，不包括数据的信息等元数据。
        关于元数据的处理将在 ``dump`` 接口中进行描述。

        以下是一些序列化的示例：
        * 对于Numpy的矩阵可以序列化为[[1,2,3],[4,5,6]]。
        * 对于Pandas的表格可以序列化为[[1,2,3],['Tom','Jack','Jenny']]。
        * 对于Pandas等具有行列名的数据结构，其行列名定义在Abstract中，而非serialized_data中。

        Returns:
            嵌套的 ``dict`` ， ``list`` ，以及具体的 ``str`` ， ``int`` ， ``float`` 等数据。
            这应该是可以被 ``json.dumps`` 处理的类型。
        """
        raise NotImplementedError

    def dump(self) -> Dict[str, Any]:
        """
        将数据进行序列化。

        dump/load用于进行数据的交互，即打造类似于pickle的功能，不过可以序列化后通过http进行传输。

        目前考虑可能要在Server端添加type信息，因此可能需要保留关键字type。

        这个接口将处理包括具体的数据本身，以及元数据在内的所有数据。
        这个接口相当于实现了 ``pickle`` 的部分功能，可以确保整个 ``Adapter`` 在处理前后保持一致。

        TODO (panhaoyu) 如果后期采用高速传输方案，将修改这个接口，将value字段设置为内存地址的标识符。

        Returns:
            所有数据的序列化。
        """
        result = self.abstract.copy()  # 这里是浅复制，因为本函数仅修改第一层对象，因此没必要采用深复制
        result['data'] = self.serialized_data
        result['type'] = self.__class__.__name__  # 这个用于在反序列化时查找值
        return result

    @classmethod
    def load(cls, data: Dict[str, Any]) -> 'BaseAdapter':
        """
        将数据进行反序列化。

        dump/load用于进行数据的交互，即打造类似于pickle的功能，不过可以序列化后通过http进行传输。

        这个接口用于解析由 ``dump`` 函数存储的数据。

        Args:
            data: 由 ``dump`` 进行导出的数据。

        Returns:
            一个基于 ``dump`` 的导出数据读取得到的新的 ``Adapter`` 的实例。

        """
        raise NotImplementedError

    def get_matrix(self, *selector: SELECTOR_TYPE) -> List[List[Any]]:
        """
        将任意数据整合成两维数据以进行显示。

        TODO (panhaoyu) 这个接口是否如此定义还需要讨论。
        目前我对于多维数组的显示的构想是，可以通过指定数据的某些维度而单独显示这个维度。
        对于一维数组和二维数组，row_index和col_index不起作用。
        对于高维数组，需要明确指出需要索引哪两个维度才能获取到数据。
        一个示例的用法是，对于6维数组，shape为(4,5,6,7,8,9)，可以使用如下方法：
            adapter.get_matrix(0,3,4,(2,6,1),(3,12,2),5,2)
        这表示对该数组进行如下操作：
            array[0,3,4,2:6:1,3:12:2,5,2]
        这对于array而言是很平常的，不过对于张量等并没有定义这些过滤过能的数据结构是比较有用的。（虽然张量八成也要采用numpy实现）

        Args:
            *selector: 分别指定各个维度的索引范围，三个值分别是start,stop,step，如果只指定了一个整数则为在该维度选定一行。

        Returns:
            二维的可以直接用于表格显示的数据。

        """
        raise NotImplementedError

    def get_header_name(self, dimension=0) -> List[str]:
        """
        获取数据在某个维度上的名称列表。

        即使是一维数组，也要同时定义dimension=0和dimension=1。
        TODO (panhaoyu) 这个接口是否如此定义还需要讨论。

        Args:
            dimension: 需要查看的维度。

        Returns:
            该维度上所有表头的名称。

        """
        raise NotImplementedError
