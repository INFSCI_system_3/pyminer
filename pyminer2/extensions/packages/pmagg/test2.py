if __name__ == '__main__':
    from PMAgg import Window
    from PyQt5 import QtGui,QtCore
    import os
    import sys
    current_path=os.getcwd()
    sys.path.append(current_path)
    import matplotlib
    matplotlib.use('module://PMAgg')

    app=Window(bg='graph')
    app.plot([1,2,3],[4,5,6])
    app.show()