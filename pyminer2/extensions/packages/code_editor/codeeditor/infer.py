from jedi import Script
source = '''
import keyword

class C:
    pass

class D:
    pass

x = D()

def f():
    pass

for variable in [keyword, f, C, x]:
    variable
'''
