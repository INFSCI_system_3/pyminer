import pandas as pd
import numpy as np
from scipy import stats
pd.set_option('precision', 8)

res=pd.DataFrame()
data = pd.read_csv('d:/demo/class.csv')
del data['Name']
del data['Sex']
for col in data.columns:
    stats_result = pd.DataFrame(data={' ': [col]})
    stats_result['Total Count'] = len(data[col])  # N 合计
    stats_result['N'] = data[col].count()  # N 非缺失
    stats_result['N*'] = data[col].isna().sum()  # N 缺失
    stats_result['CumN'] = len(data[col])  # N 合计
    stats_result['Sum'] = data[col].sum()  # 总和
    stats_result['Min'] = data[col].min()  # 最小值
    stats_result['25%'] = np.quantile(data[col],0.25,interpolation='lower')  # Q1
    stats_result['Median'] = data[col].median()  # 中位数
    stats_result['75%'] = np.quantile(data[col],0.75,interpolation='higher')  # Q3
    stats_result['Max'] = data[col].max()  # 最大值
    stats_result['Mean'] = data[col].mean()  # 均值
    stats_result['SE Mean'] = data[col].sem()  # 均值标准误 Standard deviation
    stats_result['Mean'] = stats.tmean(data[col])  # 截尾均值 ------[TO DO]待转换为内置函数导入
    stats_result['Std'] = data[col].std()  # 标准差
    stats_result['Var'] = data[col].var()  # 方差
    stats_result['CV'] = data[col].std() / data[col].mean()  # 变异系数 Coefficient of variation

    stats_result['Range'] = data[col].max() - data[col].min()  # 极差
    stats_result['QRange'] = stats_result['75%'] - stats_result['25%']  # 四分位间距 Interquartile range
    stats_result['Mode'] = data[col].mode()  # 众数
    stats_result['Sum of squares'] = data[col].apply(np.square).sum()  # 平方和 Sum of squares
    stats_result['Skew'] = data[col].skew()  # 偏度 Skewness
    stats_result['Kurt'] = data[col].kurt()  # 峰度 Kurtosis

    # 递方均差 MSSD
    a = data[col].values
    temp = pd.DataFrame(np.array([(a[i+1] - a[i]) for i in range(a.size - 1)]))
    stats_result['MSSD'] = temp.apply(np.square).sum()/(2*(len(a)-1))
    res=pd.concat([res,stats_result])

print(res)



